from django.shortcuts import render
from lab_2.models import Note

# Create your views here.


def index(request):
    note = Note.objects.all()
    response = {'note': note}
    return render(request, 'lab5_index.html', response)
