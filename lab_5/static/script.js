$(function() {
    var $notes = $('#notes');
    var note_data = '';
    $.ajax({
        url: '/lab-2/json', 
        success: function(notes) {
            $.each(notes, function (i, note) {
                note_data += '<tr>';
                note_data += '<td>' + note.fields.receiver + '</td>';
                note_data += '<td>' + note.fields.sender + '</td>';
                note_data += '<td>' + note.fields.title + '</td>';
                note_data += '<td>' + note.fields.message + '</td>';
                note_data += '</tr>';
            });
            $notes.append(note_data)
        }
    });
});