import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';

void main() {
  runApp(const KonvaSearch());
}

class KonvaSearch extends StatelessWidget {
  const KonvaSearch({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    String s1 =
        "Apa itu plasma konvalesen?\n\nPlasma adalah bagian dari darah yang mengandung antibodi. Plasma konvalensen adalah plasma darah yang diambil dari pasien yang telah dinyatakan sembuh dari Covid-19.";
    String s2 =
        "Bagaimana cara kerjanya?\n\nPlasma konvalesen dari donor diharapkan memiliki antibodi terhadap virus SARS-CoV-2 sehingga dapat menurunkan gejala, mencegah perkembangan virus, dan mempercepat proses penyembuhan pada penerima donor.";
    List<String> list = [s1, s2];
    return MaterialApp(
        theme: ThemeData(
          primarySwatch: Colors.indigo,
        ),
        home: Scaffold(
            appBar: AppBar(
              title: const Text("KonvaSearch",
                  style: TextStyle(color: Colors.red)),
            ),
            body: ListView(
              children: [
                Container(
                  alignment: Alignment.topLeft,
                  padding: const EdgeInsets.all(10.0),
                  child: const Text(
                    "\n Donasi\n Plasma\n Konvalesen",
                    style: TextStyle(fontSize: 20.0),
                  ),
                ),
                Container(
                    alignment: Alignment.topLeft,
                    padding: const EdgeInsets.all(10.0),
                    child: ElevatedButton(
                      onPressed: () {},
                      child: const Text("+ Donasi",
                          style: TextStyle(
                              fontSize: 20.0, color: Colors.yellowAccent)),
                    )),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: CarouselSlider(
                    options: CarouselOptions(
                        aspectRatio: 2.0, enlargeCenterPage: true),
                    items: list
                        .map((item) => Container(
                              child: Center(
                                  child: Text(
                                item.toString(),
                                style: const TextStyle(fontSize: 15.0),
                              )),
                              color: Colors.lightBlueAccent,
                            ))
                        .toList(),
                  ),
                ),
                Container(
                  alignment: Alignment.topLeft,
                  padding: const EdgeInsets.all(10.0),
                  child: const Text(
                    "Pendonor: 7 orang ingin mendonorkan plasmanya\nPencari Donor: 8 orang sedang mencari donor plasma",
                    style: TextStyle(fontSize: 15.0, color: Colors.red),
                  ),
                ),
              ],
            ),
            drawer: Drawer(
              child: ListView(
                padding: EdgeInsets.zero,
                children: [
                  const DrawerHeader(
                    decoration: BoxDecoration(
                      color: Colors.indigo,
                    ),
                    child: Text('KonvaSearch',
                        style: TextStyle(color: Colors.red)),
                  ),
                  ListTile(
                    title: const Text('Home'),
                    onTap: () {
                      Navigator.pop(context);
                    },
                  ),
                  ListTile(
                    title: const Text('LokasiUTD'),
                    onTap: () {
                      Navigator.pop(context);
                    },
                  ),
                  ListTile(
                    title: const Text('FAQ'),
                    onTap: () {
                      Navigator.pop(context);
                    },
                  ),
                  ListTile(
                    title: const Text('Login'),
                    onTap: () {
                      Navigator.pop(context);
                    },
                  ),
                ],
              ),
            )));
  }
}
