import 'package:flutter/material.dart';

void main() {
  runApp(const KonvaSearch());
}

class KonvaSearch extends StatefulWidget {
  const KonvaSearch({Key? key}) : super(key: key);

  @override
  _KonvaSearchState createState() => _KonvaSearchState();
}

class _KonvaSearchState extends State<KonvaSearch> {
  bool isObscure = true;
  final _formKey1 = GlobalKey<FormState>();
  final _formKey2 = GlobalKey<FormState>();
  final _messangerKey = GlobalKey<ScaffoldMessengerState>();

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        theme: ThemeData(
          primarySwatch: Colors.lightBlue,
        ),
        scaffoldMessengerKey: _messangerKey,
        home: Scaffold(
          body: Container(
            margin: const EdgeInsets.all(30.0),
            decoration: ShapeDecoration(
                shape: RoundedRectangleBorder(
              side: const BorderSide(
                  width: 4.0, style: BorderStyle.solid, color: Colors.red),
              borderRadius: BorderRadius.circular(30.0),
            )),
            child: ListView(
              children: [
                Container(
                  alignment: Alignment.topCenter,
                  padding: const EdgeInsets.all(10.0),
                  child: const Text(
                    "KonvaSearch",
                    style: TextStyle(fontSize: 50.0, color: Colors.red),
                  ),
                ),
                Form(
                  key: _formKey1,
                  child: Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: TextFormField(
                      validator: (value) {
                        if (value!.isEmpty) {
                          return "Please fill out this field";
                        } else {
                          return null;
                        }
                      },
                      decoration: const InputDecoration(
                        border: OutlineInputBorder(),
                        focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: Colors.black)),
                        hintText: "Username",
                      ),
                    ),
                  ),
                ),
                Form(
                  key: _formKey2,
                  child: Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: TextFormField(
                      validator: (value) {
                        if (value!.isEmpty) {
                          return "Please fill out this field";
                        } else {
                          return null;
                        }
                      },
                      obscureText: isObscure,
                      decoration: InputDecoration(
                          border: const OutlineInputBorder(),
                          focusedBorder: const OutlineInputBorder(
                              borderSide: BorderSide(color: Colors.black)),
                          hintText: "Password",
                          suffixIcon: IconButton(
                            icon: const Icon(Icons.visibility),
                            onPressed: () {
                              setState(() {
                                isObscure = !isObscure;
                              });
                            },
                          )),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: SizedBox(
                    height: 50,
                    child: Card(
                      color: Colors.red,
                      child: InkWell(
                        splashColor: Colors.lightBlueAccent,
                        onTap: () {
                          if (_formKey1.currentState!.validate() &
                              _formKey2.currentState!.validate()) {
                            _messangerKey.currentState!.showSnackBar(
                              const SnackBar(content: Text('Processing Data')),
                            );
                          }
                        },
                        child: const Center(
                          child: Text(
                            "Login",
                            style: TextStyle(fontSize: 20, color: Colors.white),
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
                Container(
                  alignment: Alignment.topCenter,
                  padding: const EdgeInsets.all(10.0),
                  child: const Text(
                    "Belum memiliki akun?",
                    style: TextStyle(fontSize: 15.0),
                  ),
                ),
              ],
            ),
          ),
        ));
  }
}
