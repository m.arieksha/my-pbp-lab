from lab_1.models import Friend
from django import forms

# Friend: name,npm,dob


class FriendForm(forms.ModelForm):
    class Meta:
        model = Friend
        fields = "__all__"
        error_messages = {'required': 'Please Type'}
