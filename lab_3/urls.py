from django.urls import path

from lab_1.views import index
from .views import add_friend, index

urlpatterns = [
    path('', index, name='index'),
    path('form', add_friend, name='add friend'),
]
