1.Apakah perbedaan antara JSON dan XML?

XML adalah bahasa markup, JSON, di sisi lain, adalah format data. JSON adalah pilihan tepat untuk melakukan transfer data sederhana, karena file JSON lebih kecil dibanding file XML sehingga proses transfer data lebih cepat. XML memiliki struktur dan syntax yang lebih kompleks daripada JSON, tetapi kompleksitasnyalah yang memungkinkan bahasa ini tidak hanya mentransfer data tetapi juga memproses dan memformat objek dan dokumen.

Keuntungan besar lainnya menggunakan XML adalah menangani komentar, metadata, dan ruang nama. Fitur ini memudahkan pengembang untuk melacak apa yang terjadi dan berbagi dokumen dengan anggota tim lainnya. Selain itu, XML memungkinkan berbagai tipe data (seperti gambar dan bagan), tidak seperti JSON, yang hanya mendukung string, objek, angka, dan array boolean.

Cara data disimpan dalam XML juga berbeda dari JSON. Sementara bahasa markup menyimpan data dalam struktur pohon, sebaliknya, JSON menyimpannya seperti peta, yang memerlukan pasangan nilai kunci. Selain itu, JSON tidak menggunakan tag akhir dan dapat menggunakan array (struktur data dengan kelompok elemen).

Mengenai keamanan, saat menggunakan XML, validasi DTD (Document Type Definition) dan perluasan entitas eksternal diaktifkan secara default, membuat struktur terhindar dari beberapa serangan. Menonaktifkan ini membuat struktur XML lebih aman. Di sisi lain, penggunaan JSON biasanya aman setiap saat, meskipun mungkin lebih berisiko jika menggunakan JSONP (JSON with Padding) karena dapat mengakibatkan serangan CSRF (Cross-Site Request Forgery).

Terlepas dari banyak perbedaan antara JSON dan XML, yang paling membedakannya adalah penguraian data. JSON dapat dengan mudah diuraikan oleh fungsi JavaScript biasa karena sudah terintegrasi. Hal yang sama tidak terjadi dengan XML, yang harus diurai dengan parser XML, sehingga menjadi lebih sulit dan lambat.

2.Apakah perbedaan antara HTML dan XML?

HTML dan XML saling terkait, di mana HTML menampilkan data dan menjelaskan struktur halaman web, sedangkan XML berfokus dalam menyimpan dan mentransfer data. HTML adalah bahasa markup tersendiri, sedangkan XML adalah sebuah bahasa yang menghasilkan framewrok yang mendefinisikan bahasa markup lainnya. HTML memiliki tags standarnya sendiri, sedangkan XML memiliki tags yang fleksibel yang didefinisikan beradasarkan kebutuhan programmer. Selain itu, pada HTML, kode aplikasi tambahan tidak diperlukan untuk mengurai teks. Pada XML, aplikasi XML DOM dan kode implementasi diperlukan untuk memetakan teks kembali ke objek JavaScript.

Referensi:

https://www.imaginarycloud.com/blog/json-vs-xml/
https://www.upgrad.com/blog/html-vs-xml/
