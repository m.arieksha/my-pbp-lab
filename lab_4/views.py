from django.shortcuts import redirect, render
from lab_2.models import Note
from .forms import NoteForm
# Create your views here.


def index(request):
    note = Note.objects.all()
    response = {'note': note}
    return render(request, 'lab4_index.html', response)


def add_note(request):
    form = NoteForm(request.POST or None)
    if(form.is_valid()):
        form.save()
        return redirect('index')
    else:
        form = NoteForm()

    response = {'form': form}
    return render(request, 'lab4_form.html', response)


def note_list(request):
    note = Note.objects.all()
    response = {'note': note}
    return render(request, 'lab4_note_list.html', response)
