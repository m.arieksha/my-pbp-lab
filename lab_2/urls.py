from django.urls import path
from .views import index, renderJson, renderXML

urlpatterns = [
    path('', index, name='index'),
    path('xml', renderXML, name='xml'),
    path('json', renderJson, name='json'),
]
